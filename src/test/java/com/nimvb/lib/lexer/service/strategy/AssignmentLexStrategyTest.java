package com.nimvb.lib.lexer.service.strategy;

import com.nimvb.lib.lexer.model.Token;
import com.nimvb.lib.lexer.model.enums.TokenType;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;

class AssignmentLexStrategyTest {

    private BufferedReader validSource = null;
    private BufferedReader invalidSource = null;

    @BeforeEach
    void setup() {
        String valid = "=";
        String invalid = " =";
        validSource = new BufferedReader(new StringReader(valid));
        invalidSource = new BufferedReader(new StringReader(invalid));
    }

    @Test
    void lexValidSource() throws IOException {
        AssignmentLexStrategy strategy = new AssignmentLexStrategy(validSource);
        Token                 token    = strategy.lex();
        Assertions.assertNotNull(token);
        Assertions.assertEquals(TokenType.ASSIGNMENT,token.getType());
    }

    @Test
    void lexInvalidSource() throws IOException {
        AssignmentLexStrategy strategy = new AssignmentLexStrategy(invalidSource);
        Token                 token    = strategy.lex();
        Assertions.assertNotNull(token);
        Assertions.assertEquals(TokenType.UNKNOWN,token.getType());
    }
}