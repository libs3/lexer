package com.nimvb.lib.lexer.service.builder;

import com.nimvb.lib.lexer.service.builder.core.LexerBuilder;
import com.nimvb.lib.lexer.service.core.AbstractLexer;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.lang.reflect.InvocationTargetException;

import static org.junit.jupiter.api.Assertions.*;

class DefaultLexerBuilderTest {

    @BeforeEach
    void setUp() {
    }

    @Test
    void test_buildLexer_withDefaultBuilder_shouldHaveAllDefaultLexStrategies() throws InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        LexerBuilder builder = new DefaultLexerBuilder(new ByteArrayInputStream("".getBytes()));
        AbstractLexer lexer  = builder.build();
        Assertions.assertNotNull(lexer);
        Assertions.assertEquals(10, ((DefaultLexerBuilder) builder).getStrategies().size());
    }
}