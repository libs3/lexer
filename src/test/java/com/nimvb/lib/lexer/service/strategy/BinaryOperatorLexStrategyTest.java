package com.nimvb.lib.lexer.service.strategy;

import com.nimvb.lib.base.service.io.ExtendedBufferReader;
import com.nimvb.lib.lexer.model.Token;
import com.nimvb.lib.lexer.model.enums.TokenType;
import com.nimvb.lib.lexer.service.strategy.core.LexStrategy;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

class BinaryOperatorLexStrategyTest {


    @Test
    void test_binaryOperator_withPlus_shouldReturnPlus() throws IOException {
        ExtendedBufferReader reader   = new ExtendedBufferReader(new StringReader("+"));
        LexStrategy strategy = new BinaryOperatorLexStrategy(reader);
        Token       token    = strategy.lex();
        Assertions.assertNotNull(token);
        Assertions.assertEquals(TokenType.PLUS, token.getType());
        Assertions.assertEquals(1, reader.getPosition());
    }

    @Test
    void test_binaryOperator_withMinus_shouldReturnMinus() throws IOException {
        ExtendedBufferReader reader   = new ExtendedBufferReader(new StringReader("-"));
        LexStrategy strategy = new BinaryOperatorLexStrategy(reader);
        Token       token    = strategy.lex();
        Assertions.assertNotNull(token);
        Assertions.assertEquals(TokenType.MINUS, token.getType());
        Assertions.assertEquals(1, reader.getPosition());
    }

    @Test
    void test_binaryOperator_withDiv_shouldReturnDiv() throws IOException {
        ExtendedBufferReader reader   = new ExtendedBufferReader(new StringReader("/"));
        LexStrategy strategy = new BinaryOperatorLexStrategy(reader);
        Token       token    = strategy.lex();
        Assertions.assertNotNull(token);
        Assertions.assertEquals(TokenType.DIV, token.getType());
        Assertions.assertEquals(1, reader.getPosition());
    }

    @Test
    void test_binaryOperator_withMod_shouldReturnMod() throws IOException {
        ExtendedBufferReader reader   = new ExtendedBufferReader(new StringReader("%"));
        LexStrategy strategy = new BinaryOperatorLexStrategy(reader);
        Token       token    = strategy.lex();
        Assertions.assertNotNull(token);
        Assertions.assertEquals(TokenType.MOD, token.getType());
        Assertions.assertEquals(1, reader.getPosition());
    }

    @Test
    void test_binaryOperator_withMod_shouldReturnMul() throws IOException {
        ExtendedBufferReader reader   = new ExtendedBufferReader(new StringReader("*"));
        LexStrategy strategy = new BinaryOperatorLexStrategy(reader);
        Token       token    = strategy.lex();
        Assertions.assertNotNull(token);
        Assertions.assertEquals(TokenType.MUL, token.getType());
        Assertions.assertEquals(1, reader.getPosition());
    }

    @Test
    void test_binaryOperator_withInvalid_shouldReturnUnknown() throws IOException {
        ExtendedBufferReader reader   = new ExtendedBufferReader(new StringReader("$$$"));
        LexStrategy          strategy = new BinaryOperatorLexStrategy(reader);
        Token       token    = strategy.lex();
        Assertions.assertNotNull(token);
        Assertions.assertEquals(TokenType.UNKNOWN, token.getType());
        Assertions.assertEquals(0,reader.getPosition());
    }

    @Test
    void test_binaryOperator_withMultipleOperators_shouldReturnCorrespondTokenTypesInSequence() throws IOException {
        ExtendedBufferReader reader   = new ExtendedBufferReader(new StringReader("+*/ +--"));
        LexStrategy          strategy = new BinaryOperatorLexStrategy(reader);
        List<Token> tokens = new ArrayList<>();
        do{
            Token       token    = strategy.lex();
            if(token.getType() == TokenType.UNKNOWN){
                break;
            }
            tokens.add(token);
        }while (true);

        Assertions.assertEquals(3,tokens.size());
        Assertions.assertEquals(tokens.get(0).getType(),TokenType.PLUS);
        Assertions.assertEquals(tokens.get(1).getType(),TokenType.MUL);
        Assertions.assertEquals(tokens.get(2).getType(),TokenType.DIV);
        Assertions.assertEquals(3,reader.getPosition());
    }
}