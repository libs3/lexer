package com.nimvb.lib.lexer.model;

import com.nimvb.lib.base.model.Cloneable;
import com.nimvb.lib.lexer.model.enums.TokenType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

@AllArgsConstructor
@Getter
@Builder
public class Token implements Cloneable<Token> {
    private TokenType type;
    private String    value;

    @Override
    public Token clone() {
        return new Token(this.type, this.value);
    }
}
