package com.nimvb.lib.lexer.model.enums;


import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum TokenType {
    NUMBER("[0-9]"),
    ID("\\$,[a-zA-Z0-9]"),
    PLUS("\\+"),
    MINUS("-"),
    MUL("\\*"),
    DIV("/"),
    MOD("%"),
    ASSIGNMENT("="),
    PAR_OPEN("\\("),
    PAR_CLOSE("\\)"),
    COS("cos"),
    SIN("sin"),
    SPACE("\\s"),
    SEMICOLON(";"),
    EOF(""),
    UNKNOWN("");
    private String regex;
}
