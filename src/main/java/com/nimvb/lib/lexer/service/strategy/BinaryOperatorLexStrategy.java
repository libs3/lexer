package com.nimvb.lib.lexer.service.strategy;

import com.nimvb.lib.lexer.model.Token;
import com.nimvb.lib.lexer.model.enums.TokenType;
import com.nimvb.lib.lexer.service.strategy.core.AbstractLexStrategy;

import java.io.BufferedReader;
import java.io.IOException;

public class BinaryOperatorLexStrategy extends AbstractLexStrategy {
    public BinaryOperatorLexStrategy(BufferedReader reader) {
        super(reader);
    }

    @Override
    public Token lex() throws IOException {
        Token  token   = Token.builder().type(TokenType.UNKNOWN).value("").build();
        String content = peek();
        if (content.matches(TokenType.PLUS.getRegex())) {
            read();
            token = Token.builder().type(TokenType.PLUS).value(content).build();
        }
        if (content.matches(TokenType.MINUS.getRegex())) {
            read();
            token = Token.builder().type(TokenType.MINUS).value(content).build();
        }
        if (content.matches(TokenType.DIV.getRegex())) {
            read();
            token = Token.builder().type(TokenType.DIV).value(content).build();
        }
        if (content.matches(TokenType.MUL.getRegex())) {
            read();
            token = Token.builder().type(TokenType.MUL).value(content).build();
        }
        if (content.matches(TokenType.MOD.getRegex())) {
            read();
            token = Token.builder().type(TokenType.MOD).value(content).build();
        }
        return token;
    }
}
