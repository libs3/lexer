package com.nimvb.lib.lexer.service.strategy;

import com.nimvb.lib.lexer.model.Token;
import com.nimvb.lib.lexer.model.enums.TokenType;
import com.nimvb.lib.lexer.service.strategy.core.AbstractLexStrategy;

import java.io.BufferedReader;
import java.io.IOException;

public class SinLexStrategy extends AbstractLexStrategy {
    public SinLexStrategy(BufferedReader reader) {
        super(reader);
    }

    @Override
    public Token lex() throws IOException {
        Token token = Token.builder().type(TokenType.UNKNOWN).build();
        String content = peek(TokenType.SIN.getRegex().length());
        if(content.matches(TokenType.SIN.getRegex())){
            read(TokenType.SIN.getRegex().length());
            token = Token.builder().type(TokenType.SIN).value(content).build();
        }
        return token;
    }
}
