package com.nimvb.lib.lexer.service.strategy.core;

import com.nimvb.lib.lexer.model.Token;

import java.io.IOException;

public interface LexStrategy {
    Token lex() throws IOException;
}
