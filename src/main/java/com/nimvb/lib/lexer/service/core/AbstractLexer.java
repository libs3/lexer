package com.nimvb.lib.lexer.service.core;

import com.nimvb.lib.lexer.model.Token;
import com.nimvb.lib.lexer.service.strategy.core.LexStrategy;

import java.io.BufferedReader;
import java.util.List;

public abstract class AbstractLexer implements Lexer {

    protected Token             token;
    protected BufferedReader    reader;
    protected List<LexStrategy> strategies;

    public AbstractLexer(BufferedReader reader, List<LexStrategy> strategies) {
        this.reader = reader;
        this.strategies = strategies;
    }

    public Token getToken() {
        return this.token;
    }
}
