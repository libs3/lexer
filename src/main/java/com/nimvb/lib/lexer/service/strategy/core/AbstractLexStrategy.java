package com.nimvb.lib.lexer.service.strategy.core;

import lombok.Getter;

import java.io.BufferedReader;
import java.io.IOException;

public abstract class AbstractLexStrategy implements LexStrategy {

    @Getter
    protected BufferedReader reader;

    public AbstractLexStrategy(BufferedReader reader) {
        this.reader = reader;
    }

    protected String peek() throws IOException {
        reader.mark(1);
        String content = String.valueOf(((char) reader.read()));
        reader.reset();
        return content;
    }

    protected String peek(int count) throws IOException {
        reader.mark(count);
        StringBuilder buffer = new StringBuilder();
        for (int i = 0; i < count; i++) {
            buffer.append(((char) reader.read()));
        }
        reader.reset();
        return buffer.toString();
    }

    protected int peekInt() throws IOException {
        reader.mark(1);
        int value = reader.read();
        reader.reset();
        return value;
    }

    protected void read() throws IOException {
        this.reader.read();
    }

    protected void read(int count) throws IOException {
        for (int i = 0; i < count; i++) {
            read();
        }
    }
}
