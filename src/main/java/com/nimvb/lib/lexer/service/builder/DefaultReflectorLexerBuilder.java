package com.nimvb.lib.lexer.service.builder;

import com.nimvb.lib.base.service.reflection.util.ReflectionUtil;
import com.nimvb.lib.lexer.service.Lexer;
import com.nimvb.lib.lexer.service.builder.core.LexerBuilder;
import com.nimvb.lib.lexer.service.core.AbstractLexer;
import com.nimvb.lib.lexer.service.strategy.core.AbstractLexStrategy;
import com.nimvb.lib.lexer.service.strategy.core.LexStrategy;
import lombok.Getter;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.InvocationTargetException;
import java.util.Collections;
import java.util.List;

public class DefaultReflectorLexerBuilder implements LexerBuilder {
    private final BufferedReader    reader;
    @Getter
    private       List<LexStrategy> strategies;

    public DefaultReflectorLexerBuilder(InputStream stream) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        this.reader = new BufferedReader(new InputStreamReader(stream));
        File           parent       = ReflectionUtil.parentPackageAsFile(AbstractLexStrategy.class);
        List<Class<?>> childClasses = ReflectionUtil.getChildClasses(parent, AbstractLexStrategy.class);
        for (Class<?> childClass : childClasses) {
            LexStrategy lexerStrategy = (LexStrategy) childClass.getDeclaredConstructor(BufferedReader.class).newInstance(reader);
            strategies.add(lexerStrategy);
        }
        strategies = Collections.unmodifiableList(strategies);
    }

    @Override
    public AbstractLexer build() {
        return new Lexer(reader, strategies);
    }
}
