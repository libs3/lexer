package com.nimvb.lib.lexer.service.builder;

import com.nimvb.lib.lexer.service.Lexer;
import com.nimvb.lib.lexer.service.builder.core.LexerBuilder;
import com.nimvb.lib.lexer.service.core.AbstractLexer;
import com.nimvb.lib.lexer.service.strategy.*;
import com.nimvb.lib.lexer.service.strategy.core.LexStrategy;
import lombok.Getter;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class DefaultLexerBuilder implements LexerBuilder {

    private final BufferedReader    reader;
    @Getter
    private       List<LexStrategy> strategies;

    public DefaultLexerBuilder(InputStream stream) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        this.reader = new BufferedReader(new InputStreamReader(stream));
        strategies = Collections.unmodifiableList(Arrays.asList(
                new AssignmentLexStrategy(reader),
                new BinaryOperatorLexStrategy(reader),
                new CosLexStrategy(reader),
                new EofLexStrategy(reader),
                new IdLexStrategy(reader),
                new NumberLexStrategy(reader),
                new ParenthesisLexStrategy(reader),
                new SemiColonLexStrategy(reader),
                new SinLexStrategy(reader),
                new SpaceLexStrategy(reader)
        ));
    }

    @Override
    public AbstractLexer build() {
        return new Lexer(reader, strategies);
    }
}
