package com.nimvb.lib.lexer.service.strategy;

import com.nimvb.lib.lexer.model.Token;
import com.nimvb.lib.lexer.model.enums.TokenType;
import com.nimvb.lib.lexer.service.strategy.core.AbstractLexStrategy;

import java.io.BufferedReader;
import java.io.IOException;

public class ParenthesisLexStrategy extends AbstractLexStrategy {
    public ParenthesisLexStrategy(BufferedReader reader) {
        super(reader);
    }

    @Override
    public Token lex() throws IOException {
        Token token = Token.builder().type(TokenType.UNKNOWN).build();
        String content = peek();
        if(content.matches(TokenType.PAR_OPEN.getRegex())){
            read();
            token = Token.builder().type(TokenType.PAR_OPEN).value(content).build();
        }
        if(content.matches(TokenType.PAR_CLOSE.getRegex())){
            read();
            token = Token.builder().type(TokenType.PAR_CLOSE).value(content).build();
        }
        return token;
    }
}
