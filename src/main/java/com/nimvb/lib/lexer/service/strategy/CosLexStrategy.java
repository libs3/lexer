package com.nimvb.lib.lexer.service.strategy;

import com.nimvb.lib.lexer.model.Token;
import com.nimvb.lib.lexer.model.enums.TokenType;
import com.nimvb.lib.lexer.service.strategy.core.AbstractLexStrategy;

import java.io.BufferedReader;
import java.io.IOException;

public class CosLexStrategy extends AbstractLexStrategy {
    public CosLexStrategy(BufferedReader reader) {
        super(reader);
    }

    @Override
    public Token lex() throws IOException {
        Token  token   = Token.builder().type(TokenType.UNKNOWN).value("").build();
        String content = peek(TokenType.COS.getRegex().length());
        if (content.matches(TokenType.COS.getRegex())) {
            read(TokenType.COS.getRegex().length());
            token = Token.builder().type(TokenType.COS).value(content).build();
        }
        return token;
    }
}
