package com.nimvb.lib.lexer.service.builder.core;

import com.nimvb.lib.lexer.service.core.AbstractLexer;

public interface LexerBuilder {
    AbstractLexer build();
}
