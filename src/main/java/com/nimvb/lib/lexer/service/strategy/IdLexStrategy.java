package com.nimvb.lib.lexer.service.strategy;

import com.nimvb.lib.lexer.model.Token;
import com.nimvb.lib.lexer.model.enums.TokenType;
import com.nimvb.lib.lexer.service.strategy.core.AbstractLexStrategy;

import java.io.BufferedReader;
import java.io.IOException;

public class IdLexStrategy extends AbstractLexStrategy {
    public IdLexStrategy(BufferedReader reader) {
        super(reader);
    }

    @Override
    public Token lex() throws IOException {
        Token    token        = Token.builder().type(TokenType.UNKNOWN).value("").build();
        String[] idRegexParts = TokenType.ID.getRegex().split(",");
        String   idSign       = idRegexParts[0];
        String        idRegex = idRegexParts[1];
        StringBuilder content = new StringBuilder(peek(idSign.length() + 1));
        if (content.toString().matches(idSign + idRegex)) {
            token = Token.builder().type(TokenType.ID).build();
            read(idSign.length() + 1);
            do {
                String buffer = peek();
                if (content.toString().matches(idRegex)) {
                    content.append(buffer);
                    read();
                    continue;
                }
                break;
            } while (true);
            token = Token.builder().type(token.getType()).value(content.toString()).build();
        }

        return token;
    }
}
