package com.nimvb.lib.lexer.service.core;

import java.io.IOException;

public interface Lexer {

    boolean next() throws IOException;
}
