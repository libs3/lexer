package com.nimvb.lib.lexer.service.strategy;

import com.nimvb.lib.lexer.model.Token;
import com.nimvb.lib.lexer.model.enums.TokenType;
import com.nimvb.lib.lexer.service.strategy.core.AbstractLexStrategy;

import java.io.BufferedReader;
import java.io.IOException;

public class NumberLexStrategy extends AbstractLexStrategy {
    public NumberLexStrategy(BufferedReader reader) {
        super(reader);
    }

    @Override
    public Token lex() throws IOException {
        Token         token   = Token.builder().type(TokenType.UNKNOWN).build();
        StringBuilder content = new StringBuilder(peek());
        if(content.toString().matches(TokenType.NUMBER.getRegex())){
            read();
            if(token.getType() == TokenType.UNKNOWN){
                token = Token.builder().type(TokenType.NUMBER).build();
            }
            do {
                String buffer = peek();
                if(buffer.matches(TokenType.NUMBER.getRegex())){
                    content.append(buffer);
                    read();
                    continue;
                }
                break;

            }while (true);
            token = Token.builder().type(token.getType()).value(content.toString()).build();
        }
        return token;
    }
}
