package com.nimvb.lib.lexer.service;

import com.nimvb.lib.lexer.model.Token;
import com.nimvb.lib.lexer.model.enums.TokenType;
import com.nimvb.lib.lexer.service.core.AbstractLexer;
import com.nimvb.lib.lexer.service.strategy.core.LexStrategy;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.List;

public class Lexer extends AbstractLexer {
    public Lexer(BufferedReader reader, List<LexStrategy> strategies) {
        super(reader, strategies);
    }

    @Override
    public boolean next() throws IOException {
        for (LexStrategy strategy : this.strategies) {
            token = strategy.lex();
            if(token.getType() == TokenType.EOF){
                break;
            }
            if(token.getType() != TokenType.UNKNOWN){
                if(token.getType() == TokenType.SPACE){
                    return next();
                }
                return true;
            }
        }
        return false;
    }
}
