package com.nimvb.lib.lexer.service.strategy;

import com.nimvb.lib.lexer.model.Token;
import com.nimvb.lib.lexer.model.enums.TokenType;
import com.nimvb.lib.lexer.service.strategy.core.AbstractLexStrategy;

import java.io.BufferedReader;
import java.io.IOException;

public class EofLexStrategy extends AbstractLexStrategy {
    public EofLexStrategy(BufferedReader reader) {
        super(reader);
    }

    @Override
    public Token lex() throws IOException {
        Token token = Token.builder().type(TokenType.UNKNOWN).build();
        int   value     = peekInt();
        if(value == -1){
            read();
            token = Token.builder().type(TokenType.EOF).build();
        }
        return token;
    }
}
