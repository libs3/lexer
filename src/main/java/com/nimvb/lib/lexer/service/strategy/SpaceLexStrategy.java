package com.nimvb.lib.lexer.service.strategy;

import com.nimvb.lib.lexer.model.Token;
import com.nimvb.lib.lexer.model.enums.TokenType;
import com.nimvb.lib.lexer.service.strategy.core.AbstractLexStrategy;

import java.io.BufferedReader;
import java.io.IOException;

public class SpaceLexStrategy extends AbstractLexStrategy {
    public SpaceLexStrategy(BufferedReader reader) {
        super(reader);
    }

    @Override
    public Token lex() throws IOException {
        Token token = Token.builder().type(TokenType.UNKNOWN).value("").build();

        String content = "";
        do {
            String current = peek();
            if (current.matches(TokenType.SPACE.getRegex())) {
                read();
                content += current;
                if (token.getType() == TokenType.UNKNOWN) {
                    token = Token.builder().type(TokenType.SPACE).build();
                }
            } else {
                break;
            }
        } while (true);
        token = Token.builder().type(token.getType()).value(content).build();
        return token;
    }
}
